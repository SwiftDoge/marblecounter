import React, { useState, useEffect } from "react";

interface Props { }

export const Counter: React.FC<Props> = () => {
    const [numberOfMarbles, setNumberOfMarbles] = useState<number>(0);
    const [seconds, setSeconds] = useState<number>(0);
    const [counterStarted, setCounterStarted] = useState<boolean>(false);
    const [marblesPerMinute, setMarblesPerMinute] = useState<number>(340);
    const [timeoutMs, setTimeoutMs] = useState<number>(1);
    const [remainder, setRemainder] = useState<number>(0);
    const [totalTimeout, setTotalTimeout] = useState<number>(0);
    const [currentInMs, setCurrentInMs] = useState<number>(0);
    const [lastTimout, setLastTimeout] = useState<number>(176);
    const [minutes, setMinutes] = useState<number>(0);


    const startCountHandler = () => {
        // setCounterStarted(true);
        // while (true) {
        setTimeout(() => setNumberOfMarbles(numberOfMarbles + 1), 31);
    };

    useEffect(() => {
        setTimeoutMs(60000 / marblesPerMinute);
    }, [marblesPerMinute]);

    useEffect(() => {
        if (!counterStarted) { return; }
        let rem = (remainder > 1 ? (remainder - (remainder % 1)) : 0)
        let timeout  = ((timeoutMs - (timeoutMs % 1))) + rem;

        if (remainder > 1) {
            setRemainder((remainder % 1) + (timeoutMs % 1))
        } else {
            setRemainder(remainder + (timeoutMs % 1))
        }
        const timeNow = Date.now();

        console.log("Decimal point corrected timout: " + timeout)
        console.log("current ms state: " + currentInMs);
        console.log("Ms elapsed since last timeout: " + (timeNow - currentInMs ));

        // setTotalTimeout(totalTimeout + timeout);
        let leftover = ((timeNow - currentInMs)- lastTimout);
        leftover = currentInMs == 0 ? 0 : leftover;
        console.log("the leftover: " + leftover);
        const actualTimeout = timeout - leftover;
        console.log("actualTimeout: " + actualTimeout );
        setTimeout(() => setNumberOfMarbles(numberOfMarbles + 1), actualTimeout);
        setLastTimeout(actualTimeout);
        setCurrentInMs(timeNow);
    }, [numberOfMarbles, counterStarted]);

    // useEffect(() => {setCurrentInMs(Date.now())}, [numberOfMarbles]);

    useEffect(() => {
        // if (!counterStarted || seconds >= 60) { return; }
        if (!counterStarted ) { return; }

        setTimeout(() => setSeconds(seconds + 1), 1000);
        let mod = (seconds / 60) % 1;
        // var newMinutes = mod == 0 ? seconds / 60;
        if(mod == 0){
            console.log("setting minutes to :" + seconds / 60)
            setMinutes(seconds / 60);
        }
        

        // const timer = setTimeout(() => {
        //     setTimeLeft(calculateTimeLeft());
        // }, 1000);
    }, [seconds, counterStarted]);

    useEffect(() => {
        if(minutes == 10){
            setCounterStarted(false);
        }

        console.log("setting number of marbles" + (marblesPerMinute * minutes) );
        setNumberOfMarbles(marblesPerMinute * minutes);
    }, [minutes]);

    useEffect(() => {
        console.log("Toggled time: " + Date.now ());
    }, [counterStarted]);

    return (
        <>
            <div>Current timeout : {timeoutMs}</div>
            <div>Total timeout: {totalTimeout}</div>
            <div>Number of marbles: {numberOfMarbles}</div>
            <div>Time elapsed: {seconds}</div>
            <div>Minutes passed: {minutes}</div>

            <div>
                <input type="text" value={marblesPerMinute} onChange={e => setMarblesPerMinute(parseInt(e.target.value))} placeholder="Marbles per minute" />
                <input type="button" value="Start" onClick={() => setCounterStarted(true)} />
                <input type="button" value="Stop" onClick={() => setCounterStarted(false)} />
            </div>
        </>
    );
};
